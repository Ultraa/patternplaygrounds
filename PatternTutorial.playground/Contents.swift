// ===== Strategy =====
/*
protocol SwimBehavior {
    
    func swim()
}

class ProfessionalSwimmer: SwimBehavior {
    func swim() {
        print("Professional swimming")
    }
}

class NonSwimmer: SwimBehavior {
    func swim() {
        print("Non-swimming")
    }
}

protocol Divebehavior {
    func dive()
}

class ProfessionalDiver: Divebehavior {
    
    func dive() {
        print("Professional diving")
    }
}

class NewbieDiver: Divebehavior {
    
    func dive() {
        print("Newbie diving")
    }
}

class NonDiving: Divebehavior {
    
    func dive() {
        print("Non-diving")
    }
}

class Human {
    
    private var diveBehavior: Divebehavior!
    private var swimBehavior: SwimBehavior!
    
    func performSwim() {
        
        swimBehavior.swim()
    }
    
    func performDive() {
        diveBehavior.dive()
    }
    
    func setSwimBehavior(sb: SwimBehavior) {
        
        self.swimBehavior = sb
    }
    
    func setDiveBehavior(db: Divebehavior) {
        
        self.diveBehavior = db
    }
    
    func run() {
        
        print("Running")
    }
    
    init(swimBehavior: SwimBehavior, diveBehavior: Divebehavior) {
        self.swimBehavior = swimBehavior
        self.diveBehavior = diveBehavior
    }
}

// Пример использования
let human = Human(swimBehavior: ProfessionalSwimmer(), diveBehavior: ProfessionalDiver())
human.performDive()
human.performSwim()
*/
// ===== END Strategy =====

// ===== Decorator =====
/*
protocol Porsche {
    func getPrice() -> Double
    func getDescription() -> String
}

class Boxter: Porsche {
    
    func getPrice() -> Double {
        return 120
    }
    
    func getDescription() -> String {
        return "Porsche Boxster"
    }
}

class PorscheDecorator: Porsche {
    
    private let decoratedPorsche: Porsche
    
    required init(dp: Porsche) {
        self.decoratedPorsche = dp
    }
    
    
    func getPrice() -> Double {
        return decoratedPorsche.getPrice()
    }
    
    func getDescription() -> String {
        return decoratedPorsche.getDescription()
    }
}

class PremiumAudioSystem: PorscheDecorator {
    
    required init(dp: Porsche) {
        super.init(dp: dp)
    }
    
    override func getPrice() -> Double {
        return super.getPrice() + 30
    }
    
    override func getDescription() -> String {
        return super.getDescription() + " with premium audio system"
    }
}

class PanoramicSunroof: PorscheDecorator {
    
    required init(dp: Porsche) {
        super.init(dp: dp)
    }
    
    override func getPrice() -> Double {
        return super.getPrice() + 20
    }
    
    override func getDescription() -> String {
        return super.getDescription() + " with panoramic sunroof"
    }
}

// Пример использования
var porscheBoxster: Porsche = Boxter()
porscheBoxster.getDescription()
porscheBoxster.getPrice()

porscheBoxster = PremiumAudioSystem(dp: porscheBoxster)
porscheBoxster.getDescription()
porscheBoxster.getPrice()

porscheBoxster = PanoramicSunroof(dp: porscheBoxster)
porscheBoxster.getDescription()
porscheBoxster.getPrice()

*/
// ===== END Decorator =====

// ===== Simple Factory =====
// Описание:
// Делаем класс, который может производить экземпляры других классов
/*
enum WindowType {
    case green, red
}

protocol Window {
    func color()
}

class RedWindow: Window {
    func color() {
        print("Мы создали красный ViewController")
    }
}

class GreenWindow: Window {
    func color() {
        print("Мы создали зеленый ViewController")
    }
}

let greenWindow = GreenWindow()
greenWindow.color()

let redWindow = RedWindow()
redWindow.color()

class WindowFactory {
    static func produceWindow(type: WindowType) -> Window {
        var window: Window
        
        switch type {
        case .red: window = RedWindow()
        case .green: window = GreenWindow()
        }
        return window
    }
}

// Пример использования
let fastCar2 = WindowFactory.produceWindow(type: .red)
let hugeCar2 = WindowFactory.produceWindow(type: .green)
 */
// ===== END Simple Factory =====

// ===== Factory Method =====
// Описание:
// Делаем класс, который может производить экземпляры других классов, которые производят экземпляры третьих классов,
// но уже никак не управляет этими третьими классами
/*
protocol Vehicle {
    func drive()
}

class Car: Vehicle {
    
    func drive() {
        print("drive a car")
    }
}

class Bus: Vehicle {
    
    func drive() {
        print("drive a bus")
    }
}

protocol VehicleFactory {
    func produce() -> Vehicle
}

class CarFactory: VehicleFactory {
    func produce() -> Vehicle {
        print("Car is created")
        return Car()
    }
}

class BusFactory: VehicleFactory {
    func produce() -> Vehicle {
        print("Bus is created")
        return Bus()
    }
}

// Пример использования
let carFactory = CarFactory()
let car = carFactory.produce()

let busFactory = BusFactory()
let bus = busFactory.produce()
 */
// ===== END Factory Method =====

// ===== Abstract Factory =====
// Почти то же, что и фактори метод, только тут мы можем объединить создание разных классов в одной фабрике (классе)

// ===== Singleton =====
// Используется для того, чтобы получать доступ к переменным из любой точки нашего проекта

// ===== Command ===== (Собака сидеть!) - действие, транзакция - поведенческий паттерн
/*
class Account {
    
    var accountName: String
    var balance: Int
    
    init(accountName: String, balance: Int) {
        self.accountName = accountName
        self.balance = balance
    }
}

protocol Command {
    
    var isComplete: Bool { get set }
    func execute()
}

class Deposit: Command {
    
    private var _account: Account
    private var _amount: Int
    var isComplete = false
    
    init(account: Account, amount: Int) {
        self._account = account
        self._amount = amount
    }
    
    func execute() {
        _account.balance += _amount
        isComplete = true
    }
}

class Withdraw: Command {
    
    private var _account: Account
    private var _amount: Int
    var isComplete = false
    
    init(account: Account, amount: Int) {
        self._account = account
        self._amount = amount
    }
    
    func execute() {
        if _account.balance >= _amount {
            _account.balance -= _amount
            isComplete = true
        } else {
            print("Not enough money!")
        }
    }
}

class TransactionManager {
    
    // Свойства
    static let shared = TransactionManager()
    private init() {}
    private var _transactions: [Command] = []
    
    var pendingTransactions: [Command] {
        get {
            return self._transactions.filter{ $0.isComplete == false }
        }
    }
    
    func addTransactions(command: Command) {
        self._transactions.append(command)
    }
    
    func processingTransactions() {
        // Перебираем массив, $0 - первый элемент массива
        _transactions.filter{ $0.isComplete == false }.forEach{ $0.execute() }
        // Получаем массив отфильтрованных элементов у которых isComplete == false
    }
}

// Пример использования
let account = Account(accountName: "Borisov Sergey", balance: 1000)
let transactionManager = TransactionManager.shared
transactionManager.addTransactions(command: Deposit(account: account, amount: 100))
transactionManager.addTransactions(command: Withdraw(account: account, amount: 500))
transactionManager.pendingTransactions
account.balance
transactionManager.processingTransactions()
account.balance
*/
// ===== END Command ===== Поведенческий паттерн

// ===== Adapter ===== Структурный паттерн
/*
// Класс, под который хотим адаптировать
class SimpleCar {
    
    func sound() -> String {
        return "tr-tr-tr-tr"
    }
}

// Класс, который хотим адаптировать под другой
protocol SupercarProtocol {
    
    func makeNoise() -> String
}

class Supercar: SupercarProtocol {
    
    // В дальнейшем мы хотим изменить действие вот этой функции makeNoise так, чтобы она возвращала тр-тр-тр-тр
    func makeNoise() -> String {
        return "wroom-wroom"
    }
}

// Сам адаптер, который выполнит соединение функций для разных классов
class SupercarAdaptor: SupercarProtocol {
    
    // Принимаем адаптируемый класс
    var simpleCar: SimpleCar
    
    init(simpleCar: SimpleCar) {
        self.simpleCar = simpleCar
    }
    
    func makeNoise() -> String {
        return simpleCar.sound()
    }
}

// Пример использования
let simpleCar = SimpleCar()
simpleCar.sound()
let superSimpleCar = SupercarAdaptor(simpleCar: simpleCar)
superSimpleCar.makeNoise()
*/
// ===== END Adapter =====

// ===== Facade ===== Структурный паттерн (превратить сложное в простое)
// Описание:
// Объкдиняет работу нескольких классов в одно целое

// ===== Template method =====
/*
protocol ChessFigure {
    func DoTurn()
    func BeginTurn()
    func Go()
    func EndTurn()
}

extension ChessFigure {
    func DoTurn() {
        BeginTurn()
        Go()
        EndTurn()
    }
    
    func BeginTurn() {
        preconditionFailure("this method should be overriden")
    }
    
    func Go() {
        preconditionFailure("this method should be overriden")
    }
    
    func EndTurn() {
        preconditionFailure("this method should be overriden")
    }
}

class KingFigure: ChessFigure {
    
    func BeginTurn() {
        print("Начало хода фигуры \"Король\"")
    }
    
    func Go() {
        print("Передвижение короля на клетку")
    }
    
    func EndTurn() {
        print("Конец хода фигуры \"Король\"")
    }
}

class HorseFigure: ChessFigure {
    
    func BeginTurn() {
        print("Начало хода фигуры \"Конь\"")
    }
    
    func Go() {
        print("Передвижение коня на клетку")
    }
    
    func EndTurn() {
        print("Конец хода фигуры \"Конь\"")
    }
}

let kingBlack = KingFigure()
let horseWhite = HorseFigure()

kingBlack.DoTurn()
print("===== Next turn =====")
horseWhite.DoTurn()
*/
// ===== END Template method =====

// ===== Iterator ===== - Поведенческий паттерн
/*
class Driver {
    let isGoodDriver: Bool
    let name: String
    
    init(isGood: Bool, name: String) {
        self.isGoodDriver = isGood
        self.name = name
    }
}

class CarMachine {
    var goodDriverIterator: GoodDriverIterator {
        return GoodDriverIterator(drivers: drivers)
    }
    
    private let drivers = [Driver(isGood: true, name: "Mark"),
                          Driver(isGood: false, name: "Ivan"),
                          Driver(isGood: true, name: "Anton"),
                          Driver(isGood: false, name: "Victoria"),]
}

extension CarMachine: Sequence {
    func makeIterator() -> GoodDriverIterator {
        return GoodDriverIterator(drivers: drivers)
    }
}

class GoodDriverIterator: IteratorProtocol {
    private let drivers: [Driver]
    private var current = 0
    
    init(drivers: [Driver]) {
        self.drivers = drivers.filter{ $0.isGoodDriver }
    }
    
    func next() -> Driver? {
        // Этот оператор (defer) выполняет действие в самом-самом конце
        defer { current += 1 }
        return drivers.count > current ? drivers[current] : nil
    }
    
    func allDrivers() -> [Driver] {
        return drivers
    }
}

let car2 = CarMachine()
let goodDriverIterator = car2.goodDriverIterator.next()
let goodDriverIteratorViaSequence = car2.makeIterator().allDrivers()

for driver in car2 {
    print(driver.name)
}
*/
// ===== END Iterator ===== - Поведенческий паттерн

// ===== Composite ===== - Структурный паттерн "Компоновщик"
/*
// branch
protocol Coworker {
    func hire(coworker: Coworker)
    func getInfo()
    var lvl: Int { get }
}

class Manager: Coworker {
    private var coworkers = [Coworker]()
    var lvl: Int
    
    init(lvl: Int) {
        self.lvl = lvl
    }
    
    func hire(coworker: Coworker) {
        self.coworkers.append(coworker)
    }
    func getInfo() {
        print(self.lvl.description + " level manager")
        for coworker in coworkers {
            coworker.getInfo()
        }
    }
}

// leaf
class LowLevelManager: Coworker {
    var lvl: Int
    
    init(lvl: Int) {
        self.lvl = lvl
    }
    
    func hire(coworker: Coworker) {
        print("can't hire")
    }
    
    func getInfo() {
        print(self.lvl.description + " level manager")
    }
}

let topManager = Manager(lvl: 1)
let managerLvl2 = Manager(lvl: 2)
let managerLvl3_1 = Manager(lvl: 3)
let managerLvl3_2 = Manager(lvl: 3)
let managerLvl10 = Manager(lvl: 10)

topManager.hire(coworker: managerLvl2)
managerLvl2.hire(coworker: managerLvl3_1)
managerLvl2.hire(coworker: managerLvl3_2)
managerLvl3_1.hire(coworker: managerLvl10)
print("---")
topManager.getInfo()
print("---")
managerLvl3_1.getInfo()
print("---")
print(managerLvl2.lvl)
*/
// ===== END Composite ===== - Структурный паттерн "Компоновщик"
