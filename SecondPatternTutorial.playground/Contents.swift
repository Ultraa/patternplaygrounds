// ===== State ===== - Поведенческий паттерн "Состояние"
print("===== State =====")

protocol State {
    // Перевод принтера в различные состояния
    func on(printer: Printer)
    func off(printer: Printer)
    func printDocument(printer: Printer)
}

// Состояние принтера: включен
class On: State {
    func on(printer: Printer) {
        print("It is on already")
    }
    
    func off(printer: Printer) {
        print("turning printer off")
        printer.set(state: Off())
    }
    
    func printDocument(printer: Printer) {
        print("printing")
        printer.set(state: Print())
    }
}

// Состояние принтера: выключен
class Off: State {
    func on(printer: Printer) {
        print("It is on already")
        printer.set(state: On())
    }
    
    func off(printer: Printer) {
        print("It is already off")
    }
    
    func printDocument(printer: Printer) {
        print("It is off, we can't printing")
    }
}

// Состояние принтера: идёт печать
class Print: State {
    func on(printer: Printer) {
        print("It is on already")
    }
    
    func off(printer: Printer) {
        print("It is already off")
        printer.set(state: Off())
    }
    
    func printDocument(printer: Printer) {
        print("It is already printing")
    }
}

class Printer {
    var state: State
    
    // Начальное состояние принтера при создании экземпляра класса
    init() {
        self.state = On()
    }
    
    func set(state: State) {
        self.state = state
    }
    
    func turnOn() {
        state.on(printer: self)
    }
    
    func turnOff() {
        state.off(printer: self)
    }
    
    func turnPrint() {
        state.printDocument(printer: self)
    }
}

let printer1 = Printer()
printer1.state
printer1.turnOff()
printer1.state
printer1.turnPrint()
printer1.state
printer1.turnOn()
printer1.state
printer1.turnPrint()

// ===== END State =====

// ===== Proxy ===== - Структурный паттерн

/*
class User {
    let id = "123"
}

protocol ServerProtocol {
    func grandAccess(user: User)
    func denyAccess(user: User)
}

class ServerSide: ServerProtocol {
    func grandAccess(user: User) {
        print("access granted to user with id = \(user.id)")
    }
    
    func denyAccess(user: User) {
        print("access denied to user with id = \(user.id)")
    }
}

class ServerProxy: ServerProtocol {
    // Ссылка на сервер, не создаем пока нам не понадобился доступ
    lazy private var server: ServerSide = ServerSide()
    
    func grandAccess(user: User) {
        server.grandAccess(user: user)
    }
    
    func denyAccess(user: User) {
        server.denyAccess(user: user)
    }
}

let user = User()
let proxy = ServerProxy()
proxy.grandAccess(user: user)
proxy.denyAccess(user: user)

*/
/*
class User {
//    let name = "qwerty"
//    let password = "12345"
    var name: String
    var password: String
    
    init(name: String, password: String) {
        self.name = name
        self.password = password
    }
}

protocol ServerProtocol {
    func grandAccess(user: User)
}

class ServerSide: ServerProtocol {
    func grandAccess(user: User) {
        print("access granted to user with id = \(user.name)")
    }
}

class ServerProxy: ServerProtocol {
    private var server: ServerSide!
    
    func grandAccess(user: User) {
        guard server != nil else {
            print("access can't be granted")
            return
        }
        server.grandAccess(user: user)
    }
    
    func authenticate(user: User) {
        guard user.password == "12345" else { return }
        print("user authenticated")
        server = ServerSide()
    }
}

let user1 = User(name: "Sergey", password: "12345")
let proxy = ServerProxy()
proxy.grandAccess(user: user1)
print("=== now we try to authenticate ===")
proxy.authenticate(user: user1)
proxy.grandAccess(user: user1)
*/
// ===== END Proxy =====

// ===== Builder ===== - Сложную инициализацию объекта можно проводить через "Строителя"

// ===== Chain of responsibility ===== - Поведенческий паттерн - последовательно анализирует поступающий запрос

class Enemy {
    let strength = 1600
}

protocol MilitaryChain {
    var strength: Int { get }
    var nextRank: MilitaryChain? { get set }
    
    func shouldDefeatWithStrength(amount: Int)
}

class Soldier: MilitaryChain {
    var strength = 100
    var nextRank: MilitaryChain?
    func shouldDefeatWithStrength(amount: Int) {
        if amount < strength {
            print("Солдат победил!")
        } else {
            nextRank?.shouldDefeatWithStrength(amount: amount)
        }
    }
}

class Officer: MilitaryChain {
    var strength = 500
    var nextRank: MilitaryChain?
    func shouldDefeatWithStrength(amount: Int) {
        if amount < strength {
            print("Офицер победил!")
        } else {
            nextRank?.shouldDefeatWithStrength(amount: amount)
        }
    }
}

class General: MilitaryChain {
    var strength = 1000
    var nextRank: MilitaryChain?
    func shouldDefeatWithStrength(amount: Int) {
        if amount < strength {
            print("Генерал победил!")
        } else {
            print("Враг победил!")
        }
    }
}

let enemy = Enemy()
let soldier = Soldier()
let officer = Officer()
let general = General()

soldier.nextRank = officer
officer.nextRank = general
soldier.shouldDefeatWithStrength(amount: enemy.strength)

// ===== END Chain of responsibility =====
